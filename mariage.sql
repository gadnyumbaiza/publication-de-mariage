-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 03 oct. 2021 à 05:43
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mariage`
--

-- --------------------------------------------------------

--
-- Structure de la table `tcommentaire`
--

CREATE TABLE `tcommentaire` (
  `nom` varchar(20) DEFAULT NULL,
  `telephone` varchar(13) DEFAULT NULL,
  `message` varchar(500) DEFAULT NULL,
  `date_commentaire` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `id_commentaire` int(11) NOT NULL,
  `id_mariage` int(11) DEFAULT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tcommentaire`
--

INSERT INTO `tcommentaire` (`nom`, `telephone`, `message`, `date_commentaire`, `id_commentaire`, `id_mariage`, `date_creation`, `date_modification`, `date_suppression`) VALUES
('Synthia', '+5665555', 'Je ne comprends pas comment ce gard peut se marie', '2021-10-03 03:01:42', 3, 16, '2021-10-02 20:01:42', '2021-10-02 20:01:42', NULL),
('Stereo', '+243978596585', 'Ce mariage connaitra une réussite totale. Que Dieu vous bénisse !', '2021-10-03 03:20:23', 4, 16, '2021-10-02 20:20:22', '2021-10-02 20:20:22', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tmariage`
--

CREATE TABLE `tmariage` (
  `epoux` int(11) DEFAULT NULL,
  `epouse` int(11) DEFAULT NULL,
  `date_publication` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_celebration` date NOT NULL,
  `etat_mariage` varchar(20) DEFAULT NULL,
  `id_mariage` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `date_creation` int(11) NOT NULL DEFAULT current_timestamp(),
  `date_modification` int(11) DEFAULT NULL,
  `date_suppresion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tmariage`
--

INSERT INTO `tmariage` (`epoux`, `epouse`, `date_publication`, `date_celebration`, `etat_mariage`, `id_mariage`, `id_utilisateur`, `date_creation`, `date_modification`, `date_suppresion`) VALUES
(43, 42, '2021-10-03 03:00:56', '2021-10-23', 'publié', 16, 0, 2021, 2021, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tpayement`
--

CREATE TABLE `tpayement` (
  `montant` double DEFAULT NULL,
  `date_payement` timestamp NOT NULL DEFAULT current_timestamp(),
  `id_payement` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `id_mariage` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `tpersonne`
--

CREATE TABLE `tpersonne` (
  `nom` varchar(20) DEFAULT NULL,
  `postnom` varchar(20) DEFAULT NULL,
  `prenom` varchar(20) DEFAULT NULL,
  `datenaiss` date DEFAULT NULL,
  `nompere` varchar(20) DEFAULT NULL,
  `nommere` varchar(20) DEFAULT NULL,
  `nationalite` varchar(20) DEFAULT NULL,
  `profession` varchar(20) DEFAULT NULL,
  `chefferie` varchar(20) DEFAULT NULL,
  `territoire` varchar(20) DEFAULT NULL,
  `province` varchar(20) DEFAULT NULL,
  `ville` varchar(20) DEFAULT NULL,
  `avenue` varchar(20) DEFAULT NULL,
  `numero` varchar(5) DEFAULT NULL,
  `commune` varchar(20) DEFAULT NULL,
  `photo` varchar(50) DEFAULT NULL,
  `id_personne` int(11) NOT NULL,
  `nom_marraine` varchar(20) DEFAULT NULL,
  `nom_parrain` varchar(20) DEFAULT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tpersonne`
--

INSERT INTO `tpersonne` (`nom`, `postnom`, `prenom`, `datenaiss`, `nompere`, `nommere`, `nationalite`, `profession`, `chefferie`, `territoire`, `province`, `ville`, `avenue`, `numero`, `commune`, `photo`, `id_personne`, `nom_marraine`, `nom_parrain`, `date_creation`, `date_modification`, `date_suppression`) VALUES
('MATONDO', 'Postatine', 'sandrine', '1996-02-06', 'kiodi', 'malela', 'congolaise', 'femme manager ', 'asd', 'asd', 'asd', 'asd', 'sd', 'asd', 'asd', '1633230024_7725cbf074b6fa13d906.png', 42, NULL, NULL, '2021-10-02 20:00:24', '2021-10-02 20:00:24', NULL),
('TUKATIA', 'KIOTO', 'Tresor', '1995-02-10', 'Kitoko', 'Kaioto', 'congolaise', 'Professeur', 'asd', 'ads', 'as sd', 'asd', 'asd', 'sd', 'asd', '1633230024_5cc8c0ed015b1d37af3a.png', 43, NULL, NULL, '2021-10-02 20:00:24', '2021-10-02 20:00:24', NULL);

-- --------------------------------------------------------

--
-- Structure de la table `tutilisateur`
--

CREATE TABLE `tutilisateur` (
  `nom_utilisateur` varchar(20) DEFAULT NULL,
  `mot_de_passe` varchar(127) DEFAULT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `date_creation` timestamp NOT NULL DEFAULT current_timestamp(),
  `date_modification` timestamp NULL DEFAULT NULL,
  `date_suppression` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tutilisateur`
--

INSERT INTO `tutilisateur` (`nom_utilisateur`, `mot_de_passe`, `id_utilisateur`, `date_creation`, `date_modification`, `date_suppression`) VALUES
('gadnyz', 'f7c3bc1d808e04732adf679965ccc34ca7ae3441', 1, '2021-10-02 15:01:13', '2021-10-02 17:18:46', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `tcommentaire`
--
ALTER TABLE `tcommentaire`
  ADD PRIMARY KEY (`id_commentaire`),
  ADD KEY `fk_publication` (`id_mariage`);

--
-- Index pour la table `tmariage`
--
ALTER TABLE `tmariage`
  ADD PRIMARY KEY (`id_mariage`),
  ADD KEY `fk_utilisateur2` (`id_utilisateur`),
  ADD KEY `fk_personne1` (`epoux`),
  ADD KEY `fk_personne2` (`epouse`);

--
-- Index pour la table `tpayement`
--
ALTER TABLE `tpayement`
  ADD PRIMARY KEY (`id_payement`),
  ADD KEY `fk_utlisateur` (`id_utilisateur`),
  ADD KEY `fk_mariage` (`id_mariage`);

--
-- Index pour la table `tpersonne`
--
ALTER TABLE `tpersonne`
  ADD PRIMARY KEY (`id_personne`);

--
-- Index pour la table `tutilisateur`
--
ALTER TABLE `tutilisateur`
  ADD PRIMARY KEY (`id_utilisateur`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `tcommentaire`
--
ALTER TABLE `tcommentaire`
  MODIFY `id_commentaire` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `tmariage`
--
ALTER TABLE `tmariage`
  MODIFY `id_mariage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT pour la table `tpayement`
--
ALTER TABLE `tpayement`
  MODIFY `id_payement` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT pour la table `tpersonne`
--
ALTER TABLE `tpersonne`
  MODIFY `id_personne` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT pour la table `tutilisateur`
--
ALTER TABLE `tutilisateur`
  MODIFY `id_utilisateur` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
