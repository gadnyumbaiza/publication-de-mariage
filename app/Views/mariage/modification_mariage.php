<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
  <title>Modification mariage</title>
  <link href="https://fonts.googleapis.com/css2?family=Cairo:wght@200;300;400;600;700;900&display=swap" rel="stylesheet" />

  <link href="<?= base_url() ?>/assets/css/fontawesome-all.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/css/bootstrap.min.css" rel="stylesheet">
  <link rel="stylesheet" href="<?= base_url() ?>/builder/css/tailwind.css" />
  <script src="https://cdn.jsdelivr.net/gh/alpine-collective/alpine-magic-helpers@0.5.x/dist/component.min.js"></script>
  <script src="https://cdn.jsdelivr.net/gh/alpinejs/alpine@v2.7.3/dist/alpine.min.js" defer></script>
</head>

<body>
  <div x-data="setup()" x-init="$refs.loading.classList.add('hidden'); setColors(color);" :class="{ 'dark': isDark}">
    <div class="flex h-screen antialiased text-gray-900 bg-gray-100 dark:bg-dark dark:text-light">
      <!-- Loading screen -->
      <div x-ref="loading" class="fixed inset-0 z-50 flex items-center justify-center text-2xl font-semibold text-white bg-primary-darker">
        chargement.....
      </div>
      <aside class="flex-shrink-0 hidden w-64 bg-white border-r dark:border-primary-darker dark:bg-darker md:block">
        <div class="flex flex-col h-full">
          <!-- Sidebar links -->
          <nav aria-label="Main" class="flex-1 px-2 py-4 space-y-2 overflow-y-hidden hover:overflow-y-auto">
            <!-- Dashboards links -->
            <div x-data="{ isActive: false, open: false}">
              <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
              <a href="<?= base_url() ?>/PanneauConfiguration/accueil" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                <span aria-hidden="true">
                  <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                  </svg>
                </span>
                <span class="ml-2 text-sm"> Accueil </span>
              </a>
            </div>
            <div x-data="{ isActive: false, open: false}">
              <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
              <a href="<?= base_url() ?>/PanneauConfiguration/liste_mariage" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">

                <i class="fas fa-list"></i>
                <span class="ml-2 text-sm"> Liste des mariages </span>
              </a>
            </div>

            <div x-data="{ isActive: false, open: false}">
              <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
              <a href="<?= base_url() ?>/PanneauConfiguration/nouveau_mariage" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                <i class="far fa-plus-square"></i>
                <span class="ml-2 text-sm"> Nouveau mariage</span>
              </a>
            </div>
            <div x-data="{ isActive: false, open: false}">
              <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
              <a href="<?= base_url() ?>/PanneauConfiguration/profile" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                <i class="fas fa-user"></i>
                <span class="ml-2 text-sm"> Profile </span>
              </a>
            </div>
            <div x-data="{ isActive: false, open: false}">
              <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
              <a href="<?= base_url() ?>/PanneauConfiguration/deconnexion" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                <i class="fas fa-power-off"></i>
                <span class="ml-2 text-sm"> Deconnexion </span>
              </a>
            </div>
          </nav>

          <!-- Sidebar footer -->
          <div class="flex-shrink-0 px-2 py-4 space-y-2">

            <footer class="flex items-center justify-between p-4 bg-white border-t dark:bg-darker dark:border-primary-darker">
              <div>
                Réaliser par
                <a href="#" target="_blank" class="text-blue-500 hover:underline">MARCEL MUSENSA</a>
              </div>
            </footer>
          </div>
        </div>
      </aside>

      <div class="flex-1 h-full overflow-x-hidden overflow-y-auto">
        <!-- Navbar -->
        <header class="relative bg-white dark:bg-darker">
          <div class="flex items-center justify-between p-2 border-b dark:border-primary-darker">
            <!-- Mobile menu button -->
            <button @click="isMobileMainMenuOpen = !isMobileMainMenuOpen" class="p-1 transition-colors duration-200 rounded-md text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
              <span class="sr-only">Open main manu</span>
              <span aria-hidden="true">
                <svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                </svg>
              </span>
            </button>

            <a href="<?= base_url() ?>" class="inline-block text-2xl font-bold tracking-wider uppercase text-primary-dark dark:text-light">
              Publication-mariage
            </a>

            <button @click="isMobileSubMenuOpen = !isMobileSubMenuOpen" class="p-1 transition-colors duration-200 rounded-md text-primary-lighter bg-primary-50 hover:text-primary hover:bg-primary-100 dark:hover:text-light dark:hover:bg-primary-dark dark:bg-dark md:hidden focus:outline-none focus:ring">
              <span class="sr-only">Open sub manu</span>
              <span aria-hidden="true">
                <svg class="w-8 h-8" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                  <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M12 5v.01M12 12v.01M12 19v.01M12 6a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2zm0 7a1 1 0 110-2 1 1 0 010 2z" />
                </svg>
              </span>
            </button>

            <!-- Desktop Right buttons -->
            <nav aria-label="Secondary" class="hidden space-x-2 md:flex md:items-center">

              <!-- User avatar button -->
              <div class="relative" x-data="{ open: false }">
                <button @click="open = !open; $nextTick(() => { if(open){ $refs.userMenu.focus() } })" type="button" aria-haspopup="true" :aria-expanded="open ? 'true' : 'false'" class="transition-opacity duration-200 rounded-full dark:opacity-75 dark:hover:opacity-100 focus:outline-none focus:ring dark:focus:opacity-100">
                  <span class="sr-only">Menu utilisateur</span>
                  <img class="w-10 h-10 rounded-full" src="<?= base_url() ?>/builder/images/avatar.png" alt="Ahmed Kamel" />
                </button>

                <!-- User dropdown menu -->
                <div x-show="open" x-ref="userMenu" x-transition:enter="transition-all transform ease-out" x-transition:enter-start="translate-y-1/2 opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition-all transform ease-in" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="translate-y-1/2 opacity-0" @click.away="open = false" @keydown.escape="open = false" class="absolute right-0 w-48 py-1 bg-white rounded-md shadow-lg top-12 ring-1 ring-black ring-opacity-5 dark:bg-dark focus:outline-none" tabindex="-1" role="menu" aria-orientation="vertical" aria-label="User menu">
                  <a href="<?= base_url() ?>/PanneauConfiguration/profile" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary">
                    <i class="fas fa-user"></i> Profile
                  </a>
                  <a href="<?= base_url() ?>/PanneauConfiguration/deconnexion" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary">
                    <i class="fas fa-power-off"></i>
                    Deconnexion
                  </a>
                </div>
              </div>
            </nav>

            <!-- Mobile sub menu -->
            <nav x-transition:enter="transition duration-200 ease-in-out transform sm:duration-500" x-transition:enter-start="-translate-y-full opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="-translate-y-full opacity-0" x-show="isMobileSubMenuOpen" @click.away="isMobileSubMenuOpen = false" class="absolute flex items-center p-4 bg-white rounded-md shadow-lg dark:bg-darker top-16 inset-x-4 md:hidden" aria-label="Secondary">

              <div class="relative ml-auto" x-data="{ open: false }">
                <button @click="open = !open" type="button" aria-haspopup="true" :aria-expanded="open ? 'true' : 'false'" class="block transition-opacity duration-200 rounded-full dark:opacity-75 dark:hover:opacity-100 focus:outline-none focus:ring dark:focus:opacity-100">
                  <span class="sr-only">User menu</span>
                  <img class="w-10 h-10 rounded-full" src="<?= base_url() ?>/builder/images/avatar.png" alt="Ahmed Kamel" />
                </button>
                <div x-show="open" x-transition:enter="transition-all transform ease-out" x-transition:enter-start="translate-y-1/2 opacity-0" x-transition:enter-end="translate-y-0 opacity-100" x-transition:leave="transition-all transform ease-in" x-transition:leave-start="translate-y-0 opacity-100" x-transition:leave-end="translate-y-1/2 opacity-0" @click.away="open = false" class="absolute right-0 w-48 py-1 origin-top-right bg-white rounded-md shadow-lg top-12 ring-1 ring-black ring-opacity-5 dark:bg-dark" role="menu" aria-orientation="vertical" aria-label="User menu">
                  <a href="#" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary">
                    <i class="fas fa-user"></i> Profile
                  </a>
                  <a href="<?= base_url() ?>/PanneauConfiguration/deconnexion" role="menuitem" class="block px-4 py-2 text-sm text-gray-700 transition-colors hover:bg-gray-100 dark:text-light dark:hover:bg-primary">
                    <i class="fas fa-power-off"></i>
                    Deconnexion
                  </a>
                </div>
              </div>
            </nav>
          </div>
          <!-- Mobile main manu -->
          <div class="border-b md:hidden dark:border-primary-darker" x-show="isMobileMainMenuOpen" @click.away="isMobileMainMenuOpen = false">
            <nav aria-label="Main" class="px-2 py-4 space-y-2">
              <!-- Dashboards links -->
              <div x-data="{ isActive: true, open: true}">
                <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                  <span aria-hidden="true">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 12l2-2m0 0l7-7 7 7M5 10v10a1 1 0 001 1h3m10-11l2 2m-2-2v10a1 1 0 01-1 1h-3m-6 0a1 1 0 001-1v-4a1 1 0 011-1h2a1 1 0 011 1v4a1 1 0 001 1m-6 0h6" />
                    </svg>
                  </span>
                  <span class="ml-2 text-sm"> Publication </span>
                  <span class="ml-auto" aria-hidden="true">
                    <!-- active class 'rotate-180' -->
                    <svg class="w-4 h-4 transition-transform transform" :class="{ 'rotate-180': open }" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                    </svg>
                  </span>
                </a>
                <div role="menu" x-show="open" class="mt-2 space-y-2 px-7" aria-label="Dashboards">
                  <!-- active & hover classes 'text-gray-700 dark:text-light' -->
                  <!-- inActive classes 'text-gray-400 dark:text-gray-400' -->
                  <a href="<?= base_url() ?>" role="menuitem" class="block p-2 text-sm text-gray-700 transition-colors duration-200 rounded-md dark:text-light dark:hover:text-light hover:text-gray-700">
                    Liste des publication
                  </a>
                  <a href="#" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700">
                    Nouvelle publication
                  </a>
                </div>
              </div>
              <!-- Authentication links -->
              <div x-data="{ isActive: false, open: false}">
                <!-- active & hover classes 'bg-primary-100 dark:bg-primary' -->
                <a href="#" @click="$event.preventDefault(); open = !open" class="flex items-center p-2 text-gray-500 transition-colors rounded-md dark:text-light hover:bg-primary-100 dark:hover:bg-primary" :class="{'bg-primary-100 dark:bg-primary': isActive || open}" role="button" aria-haspopup="true" :aria-expanded="(open || isActive) ? 'true' : 'false'">
                  <span aria-hidden="true">
                    <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M16 7a4 4 0 11-8 0 4 4 0 018 0zM12 14a7 7 0 00-7 7h14a7 7 0 00-7-7z" />
                    </svg>
                  </span>
                  <span class="ml-2 text-sm"> Gestion des utilisateurs </span>
                  <span aria-hidden="true" class="ml-auto">
                    <!-- active class 'rotate-180' -->
                    <svg class="w-4 h-4 transition-transform transform" :class="{ 'rotate-180': open }" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                      <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M19 9l-7 7-7-7" />
                    </svg>
                  </span>
                </a>
                <div x-show="open" class="mt-2 space-y-2 px-7" role="menu" aria-label="Authentication">
                  <a href="auth/register.html" role="menuitem" class="block p-2 text-sm text-gray-400 transition-colors duration-200 rounded-md dark:hover:text-light hover:text-gray-700">
                    Créer un utilisateur
                  </a>
                </div>
              </div>
            </nav>
          </div>
        </header>

        <!-- Main content -->
        <main>
          <!-- Content header -->
          <div class="flex items-center justify-between px-4 py-4 border-b lg:py-6 dark:border-primary-darker">
            <h1 class="text-2xl font-semibold"><a href="<?= base_url() ?>/PanneauConfiguration/accueil"><i class="fa fa-arrow-left"></i> </a> Modification mariage</h1>
          </div>

          <div class="mt-2">
            <!-- State cards -->
            <div class="grid grid-cols-1 gap-8 p-4 lg:grid-cols-12 xl:grid-cols-12">
              <div class="items-center justify-between p-4 bg-white rounded-md dark:bg-darker">
                <form enctype="multipart/form-data" method="post" action="<?= base_url() ?>/PanneauConfiguration/enregistrement_modification" class="space-y-6">
                  <div class="row">
                    <div class="col-md-6">
                      <h5>Informations de l'epoux</h5>
                      <div class="form-group">
                        <div class="d-grid text-center">
                          <img id="ajaxImgUploadEpoux" src="<?= base_url() ?>/uploads/<?= $epoux['photo'] ?>" alt="<?= base_url() ?>/uploads/<?= $epoux['photo'] ?>" width="200" />
                          <input type="text" name="id_epoux" value="<?= $epoux['id_personne'] ?>" hidden>
                        </div>
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="file" name="ImgUploadEpoux" placeholder="photo" id="ImgUploadEpoux" value="<?= $epouse['photo'] ?>" onchange="onFileUpload(this);" accept="image/*" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nom_epoux" placeholder="Nom" value="<?= $epoux['nom'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="postnom_epoux" placeholder="Postnom" value="<?= $epoux['postnom'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="prenom_epoux" placeholder="prenom" value="<?= $epoux['prenom'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="date" name="datenaiss_epoux" placeholder="date de naissance" value="<?= $epoux['datenaiss'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nompere_epoux" placeholder="nom du père" value="<?= $epoux['nompere'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nommere_epoux" placeholder="nom de la mère" value="<?= $epoux['nommere'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nationalite_epoux" placeholder="nationalité" value="<?= $epoux['nationalite'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="profession_epoux" placeholder="profession" value="<?= $epoux['profession'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="chefferie_epoux" placeholder="chefferie" value="<?= $epoux['chefferie'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="territoire_epoux" placeholder="territoire" value="<?= $epoux['territoire'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="province_epoux" placeholder="province" value="<?= $epoux['province'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="ville_epoux" placeholder="ville" value="<?= $epoux['ville'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="avenue_epoux" placeholder="avenue" value="<?= $epoux['avenue'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="numero_epoux" placeholder="numero" value="<?= $epoux['numero'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="commune_epoux" placeholder="commune" value="<?= $epoux['commune'] ?>" require />
                      </div>
                    </div>
                    <div class="col-md-6">
                      <h5>Informations de l'épouse</h5>
                      <div class="form-group">
                        <div class="d-grid text-center">
                          <img class="" id="ajaxImgUploadEpouse" src="<?= base_url() ?>/uploads/<?= $epouse['photo'] ?>" alt="<?= base_url() ?>/uploads/<?= $epouse['photo'] ?>" width="200" />
                          <input type="text" name="id_epouse" value="<?= $epouse['id_personne'] ?>" hidden>
                        </div>
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="file" name="ImgUploadEpouse" placeholder="photo" id="ImgUploadEpouse" onchange="onFileUpload(this);" accept="image/*" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nom_epouse" placeholder="Nom" value="<?= $epouse['nom'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="postnom_epouse" placeholder="Postnom" value="<?= $epouse['postnom'] ?>" require />
                      </div>
                      <div class="form-group">
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="prenom_epouse" placeholder="prenom" value="<?= $epouse['prenom'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="date" name="datenaiss_epouse" placeholder="date de naissance" value="<?= $epouse['datenaiss'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nompere_epouse" placeholder="nom du père" value="<?= $epouse['nompere'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nommere_epouse" placeholder="nom de la mère" value="<?= $epouse['nommere'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="nationalite_epouse" placeholder="nationalité" value="<?= $epouse['nationalite'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="profession_epouse" placeholder="profession" value="<?= $epouse['profession'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="chefferie_epouse" placeholder="chefferie" value="<?= $epouse['chefferie'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="territoire_epouse" placeholder="territoire" value="<?= $epouse['territoire'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="province_epouse" placeholder="province" value="<?= $epouse['province'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="ville_epouse" placeholder="ville" value="<?= $epouse['ville'] ?>" require />
                      </div>
                      <div class="form-group"><input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="avenue_epouse" placeholder="avenue" value="<?= $epouse['avenue'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="numero_epouse" placeholder="numero" value="<?= $epouse['numero'] ?>" require />
                      </div>
                      <div class="form-group">
                        <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="text" name="commune_epouse" placeholder="commune" value="<?= $epouse['commune'] ?>" require />
                      </div>

                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6">
                      <h5>Informations du mariage</h5>
                      <input class="w-full px-4 py-2 border rounded-md dark:bg-darker dark:border-gray-700 focus:outline-none
                       focus:ring focus:ring-primary-100 dark:focus:ring-primary-darker" type="date" name="date_celebration" value="<?= $mariage['date_celebration'] ?>" placeholder="Date_celebration" require />
                      <input type="text" name="id_mariage" value="<?= $mariage['id_mariage'] ?>" hidden>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12">
                      <button type="submit" class="px-4 py-2 font-medium text-center text-white transition-colors duration-200 rounded-md bg-primary hover:bg-primary-dark focus:outline-none focus:ring-2 focus:ring-primary focus:ring-offset-1 dark:focus:ring-offset-darker">
                        Enregistrer les modifications
                      </button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </main>
        <!-- Main footer -->

      </div>
      <!-- Notification panel -->
      <!-- Backdrop -->
      <div x-transition:enter="transition duration-300 ease-in-out" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition duration-300 ease-in-out" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-show="isNotificationsPanelOpen" @click="isNotificationsPanelOpen = false" class="fixed inset-0 z-10 bg-primary-darker" style="opacity: 0.5" aria-hidden="true"></div>
      <!-- Panel -->
      <section x-transition:enter="transition duration-300 ease-in-out transform sm:duration-500" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" x-ref="notificationsPanel" x-show="isNotificationsPanelOpen" @keydown.escape="isNotificationsPanelOpen = false" tabindex="-1" aria-labelledby="notificationPanelLabel" class="fixed inset-y-0 z-20 w-full max-w-xs bg-white dark:bg-darker dark:text-light sm:max-w-md focus:outline-none">
        <div class="absolute right-0 p-2 transform translate-x-full">
          <!-- Close button -->
          <button @click="isNotificationsPanelOpen = false" class="p-2 text-white rounded-md focus:outline-none focus:ring">
            <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>
        <div class="flex flex-col h-screen" x-data="{ activeTabe: 'action' }">
          <!-- Panel header -->
          <div class="flex-shrink-0">
            <div class="flex items-center justify-between px-4 pt-4 border-b dark:border-primary-darker">
              <h2 id="notificationPanelLabel" class="pb-4 font-semibold">Commentaires</h2>
              <div>
                <button @click.prevent="activeTabe = 'user'" class="px-px pb-4 transition-all duration-200 transform translate-y-px border-b focus:outline-none" :class="{'border-primary-dark dark:border-primary': activeTabe == 'user', 'border-transparent': activeTabe != 'user'}">
                  User
                </button>
              </div>
            </div>
          </div>

          <!-- Panel content (tabs) -->
          <div class="flex-1 pt-4 overflow-y-hidden hover:overflow-y-auto">
            <!-- Action tab -->
            <div class="space-y-4" x-show.transition.in="activeTabe == 'action'">
              <a href="#" class="block">
                <div class="flex px-4 space-x-4">
                  <div class="relative flex-shrink-0">
                    <span class="z-10 inline-block p-2 overflow-visible rounded-full bg-primary-50 text-primary-light dark:bg-primary-darker">
                      <svg class="w-7 h-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                      </svg>
                    </span>
                    <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                  </div>
                  <div class="flex-1 overflow-hidden">
                    <h5 class="text-sm font-semibold text-gray-600 dark:text-light">
                      New project "KWD Dashboard" created
                    </h5>
                    <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                      Looks like there might be a new theme soon
                    </p>
                    <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 9h ago </span>
                  </div>
                </div>
              </a>
              <a href="#" class="block">
                <div class="flex px-4 space-x-4">
                  <div class="relative flex-shrink-0">
                    <span class="inline-block p-2 overflow-visible rounded-full bg-primary-50 text-primary-light dark:bg-primary-darker">
                      <svg class="w-7 h-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                      </svg>
                    </span>
                    <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                  </div>
                  <div class="flex-1 overflow-hidden">
                    <h5 class="text-sm font-semibold text-gray-600 dark:text-light">
                      KWD Dashboard v0.0.2 was released
                    </h5>
                    <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                      Successful new version was released
                    </p>
                    <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 2d ago </span>
                  </div>
                </div>
              </a>
              <template x-for="i in 20" x-key="i">
                <a href="#" class="block">
                  <div class="flex px-4 space-x-4">
                    <div class="relative flex-shrink-0">
                      <span class="inline-block p-2 overflow-visible rounded-full bg-primary-50 text-primary-light dark:bg-primary-darker">
                        <svg class="w-7 h-7" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                          <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 17h5l-1.405-1.405A2.032 2.032 0 0118 14.158V11a6.002 6.002 0 00-4-5.659V5a2 2 0 10-4 0v.341C7.67 6.165 6 8.388 6 11v3.159c0 .538-.214 1.055-.595 1.436L4 17h5m6 0v1a3 3 0 11-6 0v-1m6 0H9" />
                        </svg>
                      </span>
                      <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                    </div>
                    <div class="flex-1 overflow-hidden">
                      <h5 class="text-sm font-semibold text-gray-600 dark:text-light">
                        New project "KWD Dashboard" created
                      </h5>
                      <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                        Looks like there might be a new theme soon
                      </p>
                      <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 9h ago </span>
                    </div>
                  </div>
                </a>
              </template>
            </div>

            <!-- User tab -->
            <div class="space-y-4" x-show.transition.in="activeTabe == 'user'">
              <a href="#" class="block">
                <div class="flex px-4 space-x-4">
                  <div class="relative flex-shrink-0">
                    <span class="relative z-10 inline-block overflow-visible rounded-ful">
                      <img class="object-cover rounded-full w-9 h-9" src="<?= base_url() ?>/builder/images/avatar.png" alt="image de la femme"/>
                    </span>
                    <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                  </div>
                  <div class="flex-1 overflow-hidden">
                    <h5 class="text-sm font-semibold text-gray-600 dark:text-light">Ahmed Kamel</h5>
                    <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                      Shared new project "K-WD Dashboard"
                    </p>
                    <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 1d ago </span>
                  </div>
                </div>
              </a>
              <a href="#" class="block">
                <div class="flex px-4 space-x-4">
                  <div class="relative flex-shrink-0">
                    <span class="relative z-10 inline-block overflow-visible rounded-ful">
                      <img class="object-cover rounded-full w-9 h-9" src="<?= base_url() ?>/builder/images/avatar-1.jpg" alt="Ahmed kamel" />
                    </span>
                    <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                  </div>
                  <div class="flex-1 overflow-hidden">
                    <h5 class="text-sm font-semibold text-gray-600 dark:text-light">John</h5>
                    <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                      Commit new changes to K-WD Dashboard project.
                    </p>
                    <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 10h ago </span>
                  </div>
                </div>
              </a>
              <a href="#" class="block">
                <div class="flex px-4 space-x-4">
                  <div class="relative flex-shrink-0">
                    <span class="relative z-10 inline-block overflow-visible rounded-ful">
                      <img class="object-cover rounded-full w-9 h-9" src="<?= base_url() ?>/builder/images/avatar.png" alt="Ahmed kamel" />
                    </span>
                    <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                  </div>
                  <div class="flex-1 overflow-hidden">
                    <h5 class="text-sm font-semibold text-gray-600 dark:text-light">Ahmed Kamel</h5>
                    <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                      Release new version "K-WD Dashboard"
                    </p>
                    <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 20d ago </span>
                  </div>
                </div>
              </a>
              <template x-for="i in 10" x-key="i">
                <a href="#" class="block">
                  <div class="flex px-4 space-x-4">
                    <div class="relative flex-shrink-0">
                      <span class="relative z-10 inline-block overflow-visible rounded-ful">
                        <img class="object-cover rounded-full w-9 h-9" src="<?= base_url() ?>/builder/images/avatar.png" alt="Ahmed kamel" />
                      </span>
                      <div class="absolute h-24 p-px -mt-3 -ml-px bg-primary-50 left-1/2 dark:bg-primary-darker"></div>
                    </div>
                    <div class="flex-1 overflow-hidden">
                      <h5 class="text-sm font-semibold text-gray-600 dark:text-light">Ahmed Kamel</h5>
                      <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                        Release new version "K-WD Dashboard"
                      </p>
                      <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> 20d ago </span>
                    </div>
                  </div>
                </a>
              </template>
            </div>
          </div>
        </div>
      </section>

      <!-- Search panel -->
      <!-- Backdrop -->
      <div x-transition:enter="transition duration-300 ease-in-out" x-transition:enter-start="opacity-0" x-transition:enter-end="opacity-100" x-transition:leave="transition duration-300 ease-in-out" x-transition:leave-start="opacity-100" x-transition:leave-end="opacity-0" x-show="isSearchPanelOpen" @click="isSearchPanelOpen = false" class="fixed inset-0 z-10 bg-primary-darker" style="opacity: 0.5" aria-hidden="ture"></div>
      <!-- Panel -->
      <section x-transition:enter="transition duration-300 ease-in-out transform sm:duration-500" x-transition:enter-start="-translate-x-full" x-transition:enter-end="translate-x-0" x-transition:leave="transition duration-300 ease-in-out transform sm:duration-500" x-transition:leave-start="translate-x-0" x-transition:leave-end="-translate-x-full" x-show="isSearchPanelOpen" @keydown.escape="isSearchPanelOpen = false" class="fixed inset-y-0 z-20 w-full max-w-xs bg-white shadow-xl dark:bg-darker dark:text-light sm:max-w-md focus:outline-none">
        <div class="absolute right-0 p-2 transform translate-x-full">
          <!-- Close button -->
          <button @click="isSearchPanelOpen = false" class="p-2 text-white rounded-md focus:outline-none focus:ring">
            <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
              <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
            </svg>
          </button>
        </div>

        <h2 class="sr-only">Search panel</h2>
        <!-- Panel content -->
        <div class="flex flex-col h-screen">
          <!-- Panel header (Search input) -->
          <div class="relative flex-shrink-0 px-4 py-8 text-gray-400 border-b dark:border-primary-darker dark:focus-within:text-light focus-within:text-gray-700">
            <span class="absolute inset-y-0 inline-flex items-center px-4">
              <svg class="w-5 h-5" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor">
                <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M21 21l-6-6m2-5a7 7 0 11-14 0 7 7 0 0114 0z" />
              </svg>
            </span>
            <input x-ref="searchInput" type="text" class="w-full py-2 pl-10 pr-4 border rounded-full dark:bg-dark dark:border-transparent dark:text-light focus:outline-none focus:ring" placeholder="Search..." />
          </div>

          <!-- Panel content (Search result) -->
          <div class="flex-1 px-4 pb-4 space-y-4 overflow-y-hidden h hover:overflow-y-auto">
            <h3 class="py-2 text-sm font-semibold text-gray-600 dark:text-light">History</h3>
            <a href="#" class="flex space-x-4">
              <div class="flex-shrink-0">
                <img class="w-10 h-10 rounded-lg" src="<?= base_url() ?>/builder/images/cover.jpg" alt="Post cover" />
              </div>
              <div class="flex-1 max-w-xs overflow-hidden">
                <h4 class="text-sm font-semibold text-gray-600 dark:text-light">Header</h4>
                <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                  Lorem ipsum dolor, sit amet consectetur.
                </p>
                <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> Post </span>
              </div>
            </a>
            <a href="#" class="flex space-x-4">
              <div class="flex-shrink-0">
                <img class="w-10 h-10 rounded-lg" src="<?= base_url() ?>/builder/images/avatar.png" alt="Ahmed Kamel" />
              </div>
              <div class="flex-1 max-w-xs overflow-hidden">
                <h4 class="text-sm font-semibold text-gray-600 dark:text-light">Ahmed Kamel</h4>
                <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                  Last activity 3h ago.
                </p>
                <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> Offline </span>
              </div>
            </a>
            <a href="#" class="flex space-x-4">
              <div class="flex-shrink-0">
                <img class="w-10 h-10 rounded-lg" src="<?= base_url() ?>/builder/images/cover-2.jpg" alt="K-WD Dashboard" />
              </div>
              <div class="flex-1 max-w-xs overflow-hidden">
                <h4 class="text-sm font-semibold text-gray-600 dark:text-light">K-WD Dashboard</h4>
                <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                  Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                </p>
                <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> Updated 3h ago. </span>
              </div>
            </a>
            <template x-for="i in 10" x-key="i">
              <a href="#" class="flex space-x-4">
                <div class="flex-shrink-0">
                  <img class="w-10 h-10 rounded-lg" src="<?= base_url() ?>/builder/images/cover-3.jpg" alt="K-WD Dashboard" />
                </div>
                <div class="flex-1 max-w-xs overflow-hidden">
                  <h4 class="text-sm font-semibold text-gray-600 dark:text-light">K-WD Dashboard</h4>
                  <p class="text-sm font-normal text-gray-400 truncate dark:text-primary-lighter">
                    Lorem ipsum dolor, sit amet consectetur adipisicing elit.
                  </p>
                  <span class="text-sm font-normal text-gray-400 dark:text-primary-light"> Updated 3h ago. </span>
                </div>
              </a>
            </template>
          </div>
        </div>
      </section>
    </div>
  </div>

  <!-- All javascript code in this project for now is just for demo DON'T RELY ON IT  -->
  <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.bundle.min.js"></script>
  <!-- <script src="<?= base_url() ?>/builder/js/script.js"></script> -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <script>
    const setup = () => {
      const getTheme = () => {
        if (window.localStorage.getItem('dark')) {
          return JSON.parse(window.localStorage.getItem('dark'))
        }

        return !!window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
      }

      const setTheme = (value) => {
        window.localStorage.setItem('dark', value)
      }

      const getColor = () => {
        if (window.localStorage.getItem('color')) {
          return window.localStorage.getItem('color')
        }
        return 'cyan'
      }

      const setColors = (color) => {
        const root = document.documentElement
        root.style.setProperty('--color-primary', `var(--color-${color})`)
        root.style.setProperty('--color-primary-50', `var(--color-${color}-50)`)
        root.style.setProperty('--color-primary-100', `var(--color-${color}-100)`)
        root.style.setProperty('--color-primary-light', `var(--color-${color}-light)`)
        root.style.setProperty('--color-primary-lighter', `var(--color-${color}-lighter)`)
        root.style.setProperty('--color-primary-dark', `var(--color-${color}-dark)`)
        root.style.setProperty('--color-primary-darker', `var(--color-${color}-darker)`)
        this.selectedColor = color
        window.localStorage.setItem('color', color)
        //
      }

      const updateBarChart = (on) => {
        const data = {
          data: randomData(),
          backgroundColor: 'rgb(207, 250, 254)',
        }
        if (on) {
          barChart.data.datasets.push(data)
          barChart.update()
        } else {
          barChart.data.datasets.splice(1)
          barChart.update()
        }
      }

      const updateDoughnutChart = (on) => {
        const data = random()
        const color = 'rgb(207, 250, 254)'
        if (on) {
          doughnutChart.data.labels.unshift('Seb')
          doughnutChart.data.datasets[0].data.unshift(data)
          doughnutChart.data.datasets[0].backgroundColor.unshift(color)
          doughnutChart.update()
        } else {
          doughnutChart.data.labels.splice(0, 1)
          doughnutChart.data.datasets[0].data.splice(0, 1)
          doughnutChart.data.datasets[0].backgroundColor.splice(0, 1)
          doughnutChart.update()
        }
      }

      const updateLineChart = () => {
        lineChart.data.datasets[0].data.reverse()
        lineChart.update()
      }

      return {
        loading: true,
        isDark: getTheme(),
        toggleTheme() {
          this.isDark = !this.isDark
          setTheme(this.isDark)
        },
        setLightTheme() {
          this.isDark = false
          setTheme(this.isDark)
        },
        setDarkTheme() {
          this.isDark = true
          setTheme(this.isDark)
        },
        color: getColor(),
        selectedColor: 'cyan',
        setColors,
        toggleSidbarMenu() {
          this.isSidebarOpen = !this.isSidebarOpen
        },
        isSettingsPanelOpen: false,
        openSettingsPanel() {
          this.isSettingsPanelOpen = true
          this.$nextTick(() => {
            this.$refs.settingsPanel.focus()
          })
        },
        isNotificationsPanelOpen: false,
        openNotificationsPanel() {
          this.isNotificationsPanelOpen = true
          this.$nextTick(() => {
            this.$refs.notificationsPanel.focus()
          })
        },
        isSearchPanelOpen: false,
        openSearchPanel() {
          this.isSearchPanelOpen = true
          this.$nextTick(() => {
            this.$refs.searchInput.focus()
          })
        },
        isMobileSubMenuOpen: false,
        openMobileSubMenu() {
          this.isMobileSubMenuOpen = true
          this.$nextTick(() => {
            this.$refs.mobileSubMenu.focus()
          })
        },
        isMobileMainMenuOpen: false,
        openMobileMainMenu() {
          this.isMobileMainMenuOpen = true
          this.$nextTick(() => {
            this.$refs.mobileMainMenu.focus()
          })
        },
        updateBarChart,
        updateDoughnutChart,
        updateLineChart,
      }
    }
  </script>

  <script>
    function onFileUpload(input, id) {
      id = id || '#ajax' + input.id;

      console.log(id);

      if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function(e) {
          $(id).attr('src', e.target.result).width(200)
        };

        reader.readAsDataURL(input.files[0]);
      }
    }
    $(document).ready(function() {
      $('#upload_image_form').on('submit', function(e) {

        $('.uploadBtn').html('Uploading ...');
        $('.uploadBtn').prop('Disabled');

        e.preventDefault();
        if ($('#file').val() == '') {
          alert("Choose File");
          $('.uploadBtn').html('Upload');
          $('.uploadBtn').prop('enabled');
          document.getElementById("upload_image_form").reset();
        } else {
          $.ajax({
            url: "<?php echo base_url(); ?>/AjaxFileUpload/upload",
            method: "POST",
            data: new FormData(this),
            processData: false,
            contentType: false,
            cache: false,
            dataType: "json",
            success: function(res) {
              console.log(res.success);
              if (res.success == true) {
                $('#ajaxImgUpload').attr('src', 'https://via.placeholder.com/300');
                $('#alertMsg').html(res.msg);
                $('#alertMessage').show();
              } else if (res.success == false) {
                $('#alertMsg').html(res.msg);
                $('#alertMessage').show();
              }
              setTimeout(function() {
                $('#alertMsg').html('');
                $('#alertMessage').hide();
              }, 4000);

              $('.uploadBtn').html('Upload');
              $('.uploadBtn').prop('Enabled');
              document.getElementById("upload_image_form").reset();
            }
          });
        }
      });
    });
  </script>
</body>

</html>