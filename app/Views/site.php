<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Mariage planning</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="<?= base_url() ?>/assets/img/favicon.png" rel="icon">
  <link href="<?= base_url() ?>/assets/css/fontawesome-all.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Jost:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- vendore CSS Files -->
  <link href="<?= base_url() ?>/assets/vendore/aos/aos.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/glightbox/css/glightbox.min.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?= base_url() ?>/assets/vendore/swiper/swiper-bundle.min.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?= base_url() ?>/assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Arsha - v4.3.0
  * Template URL: https://bootstrapmade.com/arsha-free-bootstrap-html-template-corporate/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="fixed-top ">
    <div class="container d-flex align-items-center">

      <h1 class="logo me-auto"><a href="index.html">Mariage Planning</a></h1>
      <!-- Uncomment below if you prefer to use an image logo -->
      <!-- <a href="index.html" class="logo me-auto"><img src="<?= base_url() ?>/assets/img/logo.png" alt="" class="img-fluid"></a>-->

      <nav id="navbar" class="navbar">
        <ul>
          <li><a class="nav-link scrollto active" href="#hero">Accueil</a></li>
          <li><a class="nav-link scrollto" href="#liste-publication">Liste publication</a></li>
        </ul>
        <i class="bi bi-list mobile-nav-toggle"></i>
      </nav><!-- .navbar -->

    </div>
  </header><!-- End Header -->

  <!-- ======= Hero Section ======= -->
  <section id="hero" class="d-flex align-items-center">

    <div class="container">
      <div class="row">
        <div class="col-lg-6 d-flex flex-column justify-content-center pt-4 pt-lg-0 order-2 order-lg-1" data-aos="fade-up" data-aos-delay="200">
          <h1>Le mariage c'est la volonté à deux de créer l'unique.</h1>
          <h2>Il n'y a rien de si beau qu'un mariage bien réglé, bien paisible.</h2>
          <!-- <div class="d-flex justify-content-center justify-content-lg-start">
            <a href="#about" class="btn-get-started scrollto">Get Started</a>
            <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" class="glightbox btn-watch-video"><i class="bi bi-play-circle"></i><span>Watch Video</span></a>
          </div> -->
        </div>
        <div class="col-lg-6 order-1 order-lg-2 hero-img" data-aos="zoom-in" data-aos-delay="200">
          <img src="<?= base_url() ?>/assets/img/hero-img.png" class="img-fluid animated" alt="">
        </div>
      </div>
    </div>

  </section><!-- End Hero -->

  <main id="main">

    <section id="liste-publication" class="team section-bg">
      <div class="container" data-aos="fade-up">

        <div class="section-title">
          <h2> Publication de mariages</h2>
          <!-- <p>Magnam dolores commodi suscipit. Necessitatibus eius consequatur ex aliquid fuga eum quidem. Sit sint consectetur velit. Quisquam quos quisquam cupiditate. Et nemo qui impedit suscipit alias ea. Quia fugiat sit in iste officiis commodi quidem hic quas.</p> -->
        </div>

        <!-- <ul class="nav nav-tabs" id="myTab" role="tablist">
          <li class="nav-item" role="presentation">
            <button class="nav-link active" id="home-tab" data-bs-toggle="tab" data-bs-target="#home" type="button" role="tab" aria-controls="home" aria-selected="true">Mariage de la semaine</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link" id="profile-tab" data-bs-toggle="tab" data-bs-target="#profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Mariage du mois</button>
          </li>
          <li class="nav-item" role="presentation">
            <button class="nav-link" id="contact-tab" data-bs-toggle="tab" data-bs-target="#contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Mariage du mois prochain</button>
          </li>
        </ul> -->
        <!-- <div class="tab-content" id="myTabContent">
          <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab"> -->
        <div class="row">
          <?php
          if (isset($mariages)) {
            if (count($mariages) > 0) {
              foreach ($mariages as $key => $mariage) { ?>

                <div class="col-lg-6">
                  <div class="member d-flex align-items-start" data-aos="zoom-in" data-aos-delay="100">
                    <table class="table table-bordered">
                      <thead>
                        <tr>
                          <th colspan="2">
                            <h4 class="text-center">Date de célébration : <?= $mariage['date_celebration'] ?></h4>
                          </th>
                        </tr>
                        <tr>
                          <th>
                            <center>
                              <div class="pic"><img src="<?= base_url() ?>/uploads/<?= $mariage['epoux']['photo'] ?>" class="img-fluid" alt=""></div>
                            </center>
                          </th>
                          <th>
                            <center>
                              <div class="pic"><img src="<?= base_url() ?>/uploads/<?= $mariage['epouse']['photo'] ?>" class="img-fluid" alt=""></div>
                            </center>
                          </th>
                        </tr>
                      </thead>
                      <tbody class="text-center">
                        <tr>
                          <td><?= $mariage['epoux']['nom'] . ' ' . $mariage['epoux']['postnom'] . ' ' . $mariage['epoux']['prenom'] ?></td>
                          <td><?= $mariage['epouse']['nom'] . ' ' . $mariage['epouse']['postnom'] . ' ' . $mariage['epouse']['prenom'] ?></td>
                        </tr>
                        <tr>
                          <td>Né le <?= $mariage['epoux']['datenaiss'] ?></td>
                          <td>Né le <?= $mariage['epouse']['datenaiss'] ?></td>
                        </tr>
                      </tbody>
                      <tfoot>
                        <tr>
                          <td colspan="2">
                            <center>
                              <a href="<?= base_url('Home/afficher/' . $mariage['id_mariage'] . '#main')  ?>" class="btn-learn-more">Voir plus</a>
                            </center>
                          </td>
                        </tr>
                      </tfoot>
                    </table>
                  </div>
                </div>
          <?php
              }
            }
          }

          ?>



        </div>
        <!-- </div>
          <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="row">

            </div>
          </div>
          <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
            <div class="row">

            </div>
          </div> -->
      </div>
    </section>
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    <div class="container footer-bottom clearfix">
      <div class="copyright">
        &copy; Copyright <strong><span>Mariage planning</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        Designed by <a href="https://bootstrapmade.com/">BootstrapMade</a>
      </div>



    </div>
  </footer><!-- End Footer -->

  <div id="preloader"></div>
  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- vendore JS Files -->
  <script src="<?= base_url() ?>/assets/vendore/aos/aos.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/glightbox/js/glightbox.min.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/php-email-form/validate.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/swiper/swiper-bundle.min.js"></script>
  <script src="<?= base_url() ?>/assets/vendore/waypoints/noframework.waypoints.js"></script>

  <!-- Template Main JS File -->
  <script src="<?= base_url() ?>/assets/js/main.js"></script>

</body>

</html>