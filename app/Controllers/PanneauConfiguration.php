<?php
//
namespace App\Controllers;

use App\Models\CommentaireModel;
use App\Models\PersonneModel;
use App\Models\MariageModel;
use App\Models\UtilisateurModel;
use App\Models\PayementModel;
use App\ThirdParty\FPDF;

//
/**
 * PanneauConfiguration
 */
class PanneauConfiguration extends BaseController
{
	/**
	 * utilisateurs
	 *
	 * @var mixed
	 */
	private $utilisateurs;
	/**
	 * utilisateurModel
	 *
	 * @var mixed
	 */
	private $utilisateurModel;
	/**
	 * commentaires
	 *
	 * @var mixed
	 */
	private $commentaires;
	/**
	 * personnes
	 *
	 * @var mixed
	 */

	private $personnes;
	/**
	 * mariages
	 *
	 * @var mixed
	 */
	private $mariages;
	/**
	 * nombres
	 *
	 * @var mixed
	 */
	private $nombres;

	/**
	 * index
	 *
	 * @return void
	 */
	public function __construct()
	{
		$this->utilisateurModel = new UtilisateurModel();
		$this->mariages = new MariageModel();
		$this->load_informations();
		helper(['form', 'url']);
	}

	/**
	 * index
	 *
	 * @return void
	 */
	public function index()
	{
		return redirect()->to(site_url('/PanneauConfiguration/accueil'));
	}


	/**
	 * load_informations
	 *
	 * @return void
	 */
	private function load_informations()
	{
		$this->utilisateurs = $this->utilisateurModel->orderBy('id_utilisateur', 'DESC')->findAll();
		$mariages = $this->mariages->findAll();
		$this->nombres = ['nombreUtilisateur' => count($this->utilisateurs), 'nombreMariage' => count($mariages)];
	}

	/**
	 * accueil
	 *
	 * @return void
	 */
	public function accueil()
	{
		return view('accueil', $this->nombres);
	}
	/**
	 * Connexion
	 *
	 * @return void
	 */
	public function Connexion()
	{

		return view('auth/connexion');
	}

	//		
	/**
	 * creer_utilisateur
	 *
	 * @return void
	 */
	public function creer_utilisateur()
	{
		return view("auth/creer_utilisateur");
	}

	//	
	/**
	 * authentifier
	 *
	 * @return void
	 */
	public function authentifier()
	{
		//Access  a la base des données

		helper(['form', 'url']);
		$session = session();

		$donnees = [
			'nom_utilisateur' => $this->request->getVar('nom_utilisateur'),
			'mot_de_passe'  => sha1($this->request->getVar('mot_de_passe')),
		];
		$connected = $this->utilisateurModel->where($donnees)->first();
		if ($connected) {
			$session->set($connected);
			// return redirect()->to(site_url('/PanneauConfiguration/accueil'));
		} else {
			$session->setFlashdata('msg', "Mot de passe ou nom d'utilisateur incorrect");
			echo view('auth/connexion');
		}
	}


	/**
	 * deconnexion
	 *
	 * @return void
	 */
	public function deconnexion()
	{
		session()->destroy();
		return redirect()->to(site_url('/PanneauConfiguration'));
	}

	//	
	/**
	 * profile
	 *
	 * @return void
	 */
	public function profile()
	{

		$id_utilisateur = session()->get('id_utilisateur');

		if ($id_utilisateur) {
			$this->utilisateurs = new UtilisateurModel();
			$utilisateur =  $this->utilisateurs->where(['id_utilisateur' =>	$id_utilisateur])->first();

			if ($utilisateur) {

				return view("profile", ['utilisateur' => $utilisateur]);
			} else
				return redirect()->to(site_url('/PanneauConfiguration/accueil'));
		}
		return redirect()->to(site_url('/PanneauConfiguration/'));
	}

	/**
	 * modifier_mot_de_passe
	 *
	 * @return void
	 */
	public function modifier_mot_de_passe()
	{
		$session = session();

		$anc_mot_de_passe =  $this->request->getVar('anc_mot_de_passe');
		$n_mot_de_passe = $this->request->getVar('n_mot_de_passe');
		$conf_mot_de_passe = $this->request->getVAR('conf_mot_de_passe');

		if (!empty($anc_mot_de_passe) && !empty($n_mot_de_passe)  && !empty($conf_mot_de_passe)) {
			if (
				sha1($anc_mot_de_passe) != session()->get("mot_de_passe") ||
				$n_mot_de_passe != $conf_mot_de_passe ||
				strlen($n_mot_de_passe) < 8
			) {

				if (sha1($anc_mot_de_passe) != session()->get("mot_de_passe")) {

					$session->setFlashdata('mot_de_passe_error', ' Le mot de passe est incorrect');
				} else if ($n_mot_de_passe != $conf_mot_de_passe) {

					$session->setFlashdata('mot_de_passe_error_conf', ' Les deux mots de passe ne concordent pas.');
				} else if (strlen($n_mot_de_passe) < 8) {

					$session->setFlashdata('mot_de_passe_error_n', ' Le mot de passe doit avoir au moins 8 caractere');
				}

				return redirect()->to(site_url('/PanneauConfiguration/profile'));
			} else {
				$this->utilisateurs = new UtilisateurModel();

				$id_utilisateur = session()->get('id_utilisateur');
				$this->utilisateurs->update($id_utilisateur, ['mot_de_passe' => sha1($n_mot_de_passe)]);

				// session()->destroy();
				$session = session();

				$connected = $this->utilisateurModel->where(['id_utilisateur' => $id_utilisateur])->first();

				if ($connected) {
					$session->set($connected);
				}

				$session->setFlashdata('mot_de_passe_update', ' <i class="fa fa-check"></i><hr> La modification du mot de passe s\'est effectuée avec succès ');
				return redirect()->to(site_url('/PanneauConfiguration/profile'));
			}
		} else
			return redirect()->to(site_url('/PanneauConfiguration/profile'));
	}

	/**
	 * modifier_mot_de_passe
	 *
	 * @return void
	 */
	public function modifier_profile()
	{
		$session = session();
		$this->utilisateurs = new UtilisateurModel();

		$nom_user = session()->get('nom_utilisateur');
		$infosprofil = [
			'nom_utilisateur' => $this->request->getVar('nom_utilisateur')
		];
		if (!empty($infosprofil['nom_utilisateur'])) {
			if ($nom_user != $infosprofil['nom_utilisateur']) {
				$id_utilisateur = session()->get('id_utilisateur');
				$this->utilisateurs->update($id_utilisateur, $infosprofil);

				// session()->destroy();

				$session = session();

				$connected = $this->utilisateurModel->where(['id_utilisateur' => $id_utilisateur])->first();

				if ($connected) {
					$session->set($connected);
				}

				$session->setFlashdata('user_name_update', '<i class="fa fa-check"></i><hr> Nouveau nom d\'utilisateur : <strong>' . $infosprofil["nom_utilisateur"] . '</strong>');

				return redirect()->to(site_url('/PanneauConfiguration/profile'));
			} else {

				$session->setFlashdata('user_name_exist', "Aucune modification apportée!");
				return redirect()->to(site_url('/PanneauConfiguration/profile'));
			}
		} else
			return redirect()->to(site_url('/PanneauConfiguration/profile'));
	}
	/**
	 * nouveau_mariage
	 *
	 * @return void
	 */
	public function nouveau_mariage()
	{
		$epouses_divorces = [];
		$epoux_divorces = [];

		$this->personnes = new PersonneModel();
		$mariages = $this->mariages->where("date_rupture is")->findAll();

		for ($i = 0; $i < count($mariages); $i++) {
			if ($mariages[$i]['date_rupture'] != NULL) {
				$epoux = $this->personnes->where(['id_personne' => $mariages[$i]['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariages[$i]['epouse']])->first();
				array_push($epouses_divorces, $epouse);
				array_push($epoux_divorces, $epoux);
			}
		}
		// var_dump($epouses_divorces);
		return view("mariage/nouveau_mariage", ['epouses_divorces' => $epouses_divorces, 'epoux_divorces' => $epoux_divorces]);
	}

	/**
	 * liste_mariage
	 *
	 * @return void
	 */
	public function liste_mariage()
	{
		$this->personnes = new PersonneModel();
		// $mariages = $this->mariages->findAll();

		$mariages = $this->mariages->where("date_rupture is NULL")->findAll();


		if (count($mariages) > 0) {

			for ($i = 0; $i < count($mariages); $i++) {
				$epoux = $this->personnes->where(['id_personne' => $mariages[$i]['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariages[$i]['epouse']])->first();
				$mariages[$i]['epoux'] = $epoux;
				$mariages[$i]['epouse'] = $epouse;
			}
		}
		return view("mariage/liste_mariage", ["mariages" => $mariages, "Essaie" => 12]);
	}

	/**
	 * enregistrement_mariage
	 *
	 * @return void
	 */
	public function enregistrement_mariage()
	{
		$session = session();
		$this->personnes = new PersonneModel();

		$photo_epouse = false;
		$photo_epoux = false;

		$id_epoux = $this->request->getVar('id_epoux');
		$id_epouse = $this->request->getVar('id_epouse');

		$infosEpouse = [];
		$infosEpoux = [];

		var_dump($id_epouse);
		var_dump($id_epoux);

		// return ;
		if (!$id_epoux) {
			// On recupere la photo du marie
			if ($photo_epoux = $this->upload('ImgUploadEpoux')) {
				//Recuperation des informations du marie
				$infosEpoux = [
					'nom' => $this->request->getVar('nom_epoux'),
					'postnom' => $this->request->getVar('postnom_epoux'),
					'prenom' => $this->request->getVar('prenom_epoux'),
					'datenaiss' => $this->request->getVar('datenaiss_epoux'),
					'nationalite' => $this->request->getVar('nationalite_epoux'),
					'nompere' => $this->request->getVar('nompere_epoux'),
					'nommere' => $this->request->getVar('nommere_epoux'),
					'province' => $this->request->getVar('province_epoux'),
					'chefferie' => $this->request->getVar('chefferie_epoux'),
					'ville' => $this->request->getVar('ville_epoux'),
					'profession' => $this->request->getVar('profession_epoux'),
					'numero' => $this->request->getVar('numero_epoux'),
					'territoire' => $this->request->getVar('territoire_epoux'),
					'commune' => $this->request->getVar('commune_epoux'),
					'avenue' => $this->request->getVar('avenue_epoux'),
					'photo' => $photo_epoux
				];

				//on verifie que l'epoux n'exite pas encore
				if ($epoux_trouve =  $this->personne_exist(array_slice($infosEpoux, 0, 3))) {

					$session->setFlashdata('erreur_enr', "les informations de l'epoux " . strtoupper($infosEpoux['nom']  . " " . $infosEpoux['postnom'] . " " . $infosEpoux['prenom']) . " existent déjà");
					return redirect()->to(site_url('/PanneauConfiguration/nouveau_mariage'));
				}
			} else {
				//Retour a l'interface d'ajout d'un mariage en afficahnt un message d'erreur de chargement de la photo
				$session->setFlashdata('erreur_enr', "Echec de chargement de la photo de l'epoux");
				return redirect()->to(site_url('/PanneauConfiguration/nouveau_mariage'));
			}
		}
		if (!$id_epouse) {
			// On recupere la photo de la mariee
			if ($photo_epouse = $this->upload('ImgUploadEpouse')) {
				//Recuperation des informations de la mariee
				$infosEpouse = [
					'nom' => $this->request->getVar('nom_epouse'),
					'postnom' => $this->request->getVar('postnom_epouse'),
					'prenom' => $this->request->getVar('prenom_epouse'),
					'datenaiss' => $this->request->getVar('datenaiss_epouse'),
					'nationalite' => $this->request->getVar('nationalite_epouse'),
					'nompere' => $this->request->getVar('nompere_epouse'),
					'nommere' => $this->request->getVar('nommere_epouse'),
					'province' => $this->request->getVar('province_epouse'),
					'chefferie' => $this->request->getVar('chefferie_epouse'),
					'ville' => $this->request->getVar('ville_epouse'),
					'profession' => $this->request->getVar('profession_epouse'),
					'numero' => $this->request->getVar('numero_epouse'),
					'commune' => $this->request->getVar('commune_epouse'),
					'territoire' => $this->request->getVar('territoire_epouse'),
					'avenue' => $this->request->getVar('avenue_epouse'),
					'photo' => $photo_epouse
				];

				//on verifie que la marie n'exite pas encore
				if ($epouse_trouve = $this->personne_exist(array_slice($infosEpouse, 0, 3))) {
					$session->setFlashdata('erreur_enr', "les informations de l'epouse " . strtoupper($infosEpouse['nom']  . " " . $infosEpouse['postnom'] . " " . $infosEpouse['prenom']) . " existent déjà");
					return redirect()->to(site_url('/PanneauConfiguration/nouveau_mariage'));
				}
			} else {
				//Retour a l'interface d'ajout d'un mariage en afficahnt un message d'erreur de chargement de la photo
				$session->setFlashdata('erreur_enr', "Echec de chargement de la photo de l'epouse");
				return redirect()->to(site_url('/PanneauConfiguration/nouveau_mariage'));
			}
		}

		//On insert les informations de l'epoux et de l'epouse que lorsque le deux n'existent pas dans al base des donnees
		if (!$epoux_trouve && !$epouse_trouve) {
			$id_epouse = $this->personnes->insert($infosEpouse);
			$id_epoux = $this->personnes->insert($infosEpoux);
		}

		$donnees = ['epoux' => $id_epoux, 'epouse' => $id_epouse, 'etat_mariage' => 'en attente', 'date_celebration' => $this->request->getVar('date_celebration')];

		//On verifie que nous disposons de l'id du marie et de celui de la mariee
		if ($id_epouse && $id_epoux) {
			if ($this->mariages->insert($donnees)) {
				$session->setFlashdata('success_enr', '<i class="fa fa-check"> </i> L\'enregistrement du mariage s\'est effectué avec succès !');
				return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
			}
		}
	}

	public function personne_exist($donnees)
	{
		$personnes = new PersonneModel();
		if ($personnes->where($donnees)->first())
			return true;
		return false;
	}

	/**
	 * details
	 *
	 * @param  mixed $id_mariage
	 * @return void
	 */
	public function details($id_mariage = "")
	{

		$epoux = [];
		$epouse = [];
		$payement = [];
		$mariage = [];

		if (!empty($id_mariage)) {
			$this->personnes = new PersonneModel();
			$this->commentaires = new CommentaireModel();
			$this->payements = new PayementModel();
			$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
			if ($mariage) {
				$commentaires =  $this->commentaires->where(['id_mariage' => $mariage['id_mariage']])->orderBy('date_commentaire', 'DESC')->findAll();
				$epoux = $this->personnes->where(['id_personne' => $mariage['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariage['epouse']])->first();
				$payement = $this->payements->where(['id_mariage' => $mariage['id_mariage']])->orderBy('date_payement', 'DESC')->findAll();
				$donnees = ['mariage' => $mariage, 'epoux' => $epoux, 'epouse' => $epouse, 'payement' => $payement, 'commentaires' => $commentaires];

				if ($payement) {
					// var_dump($payement);
					$donnees['deja_paye'] = 'no';
				}
				return view("mariage/details_mariage", $donnees);
			} else {
				return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
			}
		} else
			return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
	}

	/**
	 * enregistrement_payement
	 *
	 * @return void
	 */
	public function enregistrement_payement()
	{
		$this->payements = new PayementModel();

		$infos_payement = [
			'id_mariage' => $this->request->getVar('id_mariage'),
			'montant' => $this->request->getVar('montant')
		];

		$id_payement = $this->payements->insert($infos_payement);

		return redirect()->to(site_url('/PanneauConfiguration/details/' . $infos_payement['id_mariage']));
	}

	/**
	 * publier_mariage
	 *
	 * @param  mixed $id_mariage
	 * @return void
	 */
	public function publier_mariage($id_mariage = "")
	{
		if (!empty($id_mariage)) {
			$data = [
				'etat_mariage' => "publié",
			];

			$this->mariages->update($id_mariage, $data);
			return redirect()->to(site_url('/PanneauConfiguration/details/' . $id_mariage));
		}
		return redirect()->to(site_url('/PanneauConfiguration/accueil/'));
	}

	/**
	 * annuler_publication
	 *
	 * @param  mixed $id_mariage
	 * @return void
	 */
	public function annuler_publication($id_mariage = "")
	{
		if (!empty($id_mariage)) {
			$data = [
				'etat_mariage' => "en attente",
			];

			$this->mariages->update($id_mariage, $data);
			return redirect()->to(site_url('/PanneauConfiguration/details/' . $id_mariage));
		}
		return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
	}

	/**
	 * rompre_mariage
	 *
	 * @param  mixed $id_mariage
	 * @return void
	 */
	public function rompre_mariage($id_mariage = "")
	{
		if (!empty($id_mariage)) {
			$data = [
				'date_rupture' => date('Y-m-d')
			];
			$this->mariages->update($id_mariage, $data);
		}
		return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
	}


	/**
	 * supprimer_mariage
	 *
	 * @param  mixed $id_mariage
	 * @return void
	 */
	public function supprimer_mariage($id_mariage = "")
	{
		if (!empty($id_mariage)) {

			$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
			if ($mariage) {
				$this->mariages->delete(['id_mariage' => $id_mariage]);
			}

			return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
		}
		return redirect()->to(site_url('/PanneauConfiguration/accueil/'));
	}

	/**
	 * supprimer_payement
	 *
	 * @param  mixed $id_payement
	 * @return void
	 */
	public function supprimer_payement($id_payement = "")
	{
		if (!empty($id_payement)) {

			$this->payements = new PayementModel();
			$payement = $this->payements->where(['id_payement' => $id_payement])->first();
			if ($payement) {
				$id_mariage = $payement['id_mariage'];

				$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
				if ($mariage) {
					$this->payements->delete(['id_payement' => $id_payement]);
				}
				return redirect()->to(site_url('/PanneauConfiguration/details/' . $id_mariage));
			}

			return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
		}
		return redirect()->to(site_url('/PanneauConfiguration/accueil/'));
	}

	/**
	 * upload
	 *
	 * @return void
	 */
	public function upload($fichier = 'ImgUploadEpoux')
	{
		helper(['form', 'url']);
		if (($image = $this->request->getFile($fichier))) {
			if (($image->isValid() && !$image->hasMoved())) {
				$nom = $image->getRandomName();
				$image->move('../public/uploads', $nom);
				return $nom;
			} else {
				return null;
			}
		} else {
			return null;
		}
	}

	public function imprimer_mariage($id_mariage = "")
	{
		$epoux = [];
		$epouse = [];
		$mariage = [];

		if (!empty($id_mariage)) {
			$this->personnes = new PersonneModel();
			$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
			if ($mariage) {
				$epoux = $this->personnes->where(['id_personne' => $mariage['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariage['epouse']])->first();

				$pdf = new FPDF();

				$pdf->AddPage();
				$pdf->SetFont('Times', '', 12);
				$pdf->Cell(0, 10, utf8_decode('REPUBLIQUE DÉMOCRATIQUE DU CONGO'), 0, 1, 'C');
				$pdf->Cell(0, 10, 'PROVINCE DU NORD-KIVU', 0, 1);
				$pdf->Cell(0, 10, 'VILLE DE GOMA  ', 0, 1);
				$pdf->Cell(0, 10, 'COMMUNE DE GOMA', 0, 1);
				$pdf->Cell(0, 10, 'Email : communedegoma@yahoo.fr', 0, 1);

				$pdf->SetFont('Times', 'BU', 12);
				$pdf->Cell(0, 10, 'PUBLICATION DE MARIAGE', 0, 1, 'C');

				$pdf->SetFont('Times', '', 12);

				$contenu =
					"
				Il est porté à la connaissance du public qu il sera célébré à l office de l Etat-civil de la commune de Goma à Goma le " . $mariage['date_celebration'] . " à 09 Heure, le mariage monogamique civil projeté entre : Monsieur " . strtoupper($epoux['nom']) . "  " . $epoux['postnom'] . "  " . strtoupper($epoux['prenom']) . " Né le " . strtoupper($epoux['datenaiss']) . " Fils de " . strtoupper($epoux['nompere']) . " Et de " . strtoupper($epoux['nommere']) . " de nationalité " . strtoupper($epoux['nationalite']) . " , Profession " . strtoupper($epoux['profession']) . " , originaire de la chefferie de " . strtoupper($epoux['chefferie']) . ", territoire de " . strtoupper($epoux['territoire']) . ", Province du " . strtoupper($epoux['province']) . " , resident sur l'avenue " . strtoupper($epoux['avenue']) . ", numéro " . strtoupper($epoux['numero']) . ", en commune de" . strtoupper($epoux['commune']) . " d une part
				
				Et Mademoiselle  " . strtoupper($epouse['nom']) . "  " . $epouse['postnom'] . "  " . strtoupper($epouse['prenom']) . " Née le " . strtoupper($epouse['datenaiss']) . " Fille de de " . strtoupper($epouse['nompere']) . " Et de " . strtoupper($epouse['nommere']) . " de nationalité " . strtoupper($epouse['nationalite']) . " , Profession " . strtoupper($epouse['profession']) . " , originaire de la chefferie de " . strtoupper($epouse['chefferie']) . ", territoire de " . strtoupper($epouse['territoire']) . ", Province du " . strtoupper($epouse['province']) . " , resident sur l'avenue " . strtoupper($epouse['avenue']) . ", numéro " . strtoupper($epouse['numero']) . ", en commune de" . strtoupper($epouse['commune']) . " d autre part.
				
				Quiconque aurait une opposition quelconque à cet effet, doit nous communiquer par écrit ou verbalement avant la date de célébration officielle";


				$pdf->MultiCell(0, 10, utf8_decode($contenu), 0);
				$pdf->Ln(20);
				$pdf->SetFont('Times', 'B', 12);
				$pdf->Cell(0, 10, 'Fait à Goma, le ' . date('d/m/Y'), 0, 1, 'C');
				$pdf->Cell(0, 10, 'L OFFICIER DE L ETAT-CIVIL', 0, 1, 'C');
				$pdf->Cell(0, 10, 'ET BOURGMESTRE DE LA COMMUNE DE GOMA', 0, 1, 'C');


				$this->response->setHeader('Content-Type', 'application/pdf');

				$pdf->Output();
			} else {
				return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
			}
		} else
			return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
	}

	public function essaie()
	{
		$mariage['date_celebration'] = "08/12/2021";
		$annee = date("Y", strtotime($mariage['date_celebration']));
		echo $annee;
	}

	public function modifier_mariage($id_mariage = "")
	{
		$epoux = [];
		$epouse = [];
		$mariage = [];

		if (!empty($id_mariage)) {
			$this->personnes = new PersonneModel();
			$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
			if ($mariage) {
				$epoux = $this->personnes->where(['id_personne' => $mariage['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariage['epouse']])->first();
				$donnees = ['mariage' => $mariage, 'epoux' => $epoux, 'epouse' => $epouse];

				return view("mariage/modification_mariage", $donnees);
			} else {
				return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
			}
		} else
			return redirect()->to(site_url('/PanneauConfiguration/liste_mariage'));
	}

	public function enregistrement_modification()
	{
		$session = session();
		$imageEpoux = $this->request->getFile('ImgUploadEpoux');
		$imageEpouse = $this->request->getFile('ImgUploadEpouse');

		$infosEpoux = [
			'nom' => $this->request->getVar('nom_epoux'),
			'postnom' => $this->request->getVar('postnom_epoux'),
			'prenom' => $this->request->getVar('prenom_epoux'),
			'datenaiss' => $this->request->getVar('datenaiss_epoux'),
			'nationalite' => $this->request->getVar('nationalite_epoux'),
			'nompere' => $this->request->getVar('nompere_epoux'),
			'nommere' => $this->request->getVar('nommere_epoux'),
			'province' => $this->request->getVar('province_epoux'),
			'chefferie' => $this->request->getVar('chefferie_epoux'),
			'ville' => $this->request->getVar('ville_epoux'),
			'profession' => $this->request->getVar('profession_epoux'),
			'numero' => $this->request->getVar('numero_epoux'),
			'territoire' => $this->request->getVar('territoire_epoux'),
			'commune' => $this->request->getVar('commune_epoux'),
			'avenue' => $this->request->getVar('avenue_epoux')
		];

		$infosEpouse = [
			'nom' => $this->request->getVar('nom_epouse'),
			'postnom' => $this->request->getVar('postnom_epouse'),
			'prenom' => $this->request->getVar('prenom_epouse'),
			'datenaiss' => $this->request->getVar('datenaiss_epouse'),
			'nationalite' => $this->request->getVar('nationalite_epouse'),
			'nompere' => $this->request->getVar('nompere_epouse'),
			'nommere' => $this->request->getVar('nommere_epouse'),
			'province' => $this->request->getVar('province_epouse'),
			'chefferie' => $this->request->getVar('chefferie_epouse'),
			'ville' => $this->request->getVar('ville_epouse'),
			'profession' => $this->request->getVar('profession_epouse'),
			'numero' => $this->request->getVar('numero_epouse'),
			'commune' => $this->request->getVar('commune_epouse'),
			'territoire' => $this->request->getVar('territoire_epouse'),
			'avenue' => $this->request->getVar('avenue_epouse')
		];

		if (!empty($imageEpoux->getName()) || !empty($imageEpouse->getName())) {
			$photos = $this->upload();
			if (!$photos) {
				echo 'image invalid';
				return;
			}
			$infosEpoux['photo'] = $photos['epoux'];
			$infosEpouse['photo'] = $photos['epouse'];
		}

		//Appel aux models
		$this->personnes = new PersonneModel();
		$this->mariages = new MariageModel();

		//recuperation des Ids
		$id_epouse = $this->request->getVar('id_epouse');
		$id_epoux = $this->request->getVar('id_epoux');
		$id_mariage = $this->request->getVar('id_mariage');

		//modification des informations de l'epoux et celles de l'epouse
		$this->personnes->update($id_epouse, $infosEpouse);
		$this->personnes->update($id_epoux, $infosEpoux);
		//modification de la date de celebration du mariage
		$donnees = ['date_celebration' => $this->request->getVar('date_celebration')];
		if ($this->mariages->update($id_mariage, $donnees)) {
			$session->setFlashdata('success_update', '<i class="fa fa-check"> </i> La modification du mariage s\'est effectuée avec succès !');
			return redirect()->to(site_url('/PanneauConfiguration/details/' . $id_mariage));
		} else {
			return redirect()->to(site_url('/PanneauConfiguration/modifier_mariage/' . $id_mariage));
		}
	}
}
