<?php
//
namespace App\Controllers;

use App\Models\UtilisateurModel;
//
class AdminAccess extends BaseController
{
    /**
     * index
     *
     * @return void
     */

    /**
     * index
     *
     * @return void
     */
    public function index()
    {

        if (!session()->get('nom_utilisateur')) {
            helper(['form', 'url']);
            echo view('auth/connexion');
        } else return redirect()->to(site_url('/PanneauConfiguration'));
    }



    //	
    /**
     * authentifier
     *
     * @return void
     */
    public function authentifier()
    {
        $this->utilisateurs = new UtilisateurModel();
        //Access  a la base des données

        helper(['form', 'url']);
        $session = session();

        $donnees = [
            'nom_utilisateur' => $this->request->getVar('nom_utilisateur'),
            'mot_de_passe'  => sha1($this->request->getVar('mot_de_passe')),
        ];
        $connected = $this->utilisateurs->where($donnees)->first();
        if ($connected) {
            $session->set($connected);
            return redirect()->to(site_url('/PanneauConfiguration'));
        } else {
            $session->setFlashdata('msg', "Mot de passe ou nom d'utilisateur incorrect");
            echo view('auth/connexion');
        }
    }
}
