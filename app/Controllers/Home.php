<?php
//
namespace App\Controllers;

use App\Models\CommentaireModel;
use App\Models\MariageModel;
use App\Models\PersonneModel;
use App\Models\UtilisateurModel;
use CodeIgniter\I18n\Time;
//
class Home extends BaseController
{
	private $mariages;
	/**
	 * index
	 *
	 * @return void
	 */
	public function index()
	{
		$this->load_informations();
		return view("site", ['mariages' => $this->mariages]);
	}

	public function load_informations()
	{
		$mariages = new MariageModel();
		$where = "date_celebration >=CURDATE()";
		$mariages = $mariages->where([$where, 'etat_mariage' => "publié"])->findAll();

		$this->personnes = new PersonneModel();

		for ($i = 0; $i < count($mariages); $i++) {
			$epoux = $this->personnes->where(['id_personne' => $mariages[$i]['epoux']])->first();
			$epouse = $this->personnes->where(['id_personne' => $mariages[$i]['epouse']])->first();
			$mariages[$i]['epoux'] = $epoux;
			$mariages[$i]['epouse'] = $epouse;
			$mariages[$i]['date_celebration'] = date(" d-m-Y", strtotime($mariages[$i]['date_celebration']));
		}

		$this->mariages = $mariages;
	}

	public function afficher($id_mariage = "")
	{

		$epoux = [];
		$epouse = [];
		$mariage = [];

		if (!empty($id_mariage)) {
			$this->mariages = new MariageModel();
			$this->personnes = new PersonneModel();
			$mariage = $this->mariages->where(['id_mariage' => $id_mariage])->first();
			if ($mariage) {
				$epoux = $this->personnes->where(['id_personne' => $mariage['epoux']])->first();
				$epouse = $this->personnes->where(['id_personne' => $mariage['epouse']])->first();
				$donnees = ['mariage' => $mariage, 'epoux' => $epoux, 'epouse' => $epouse];

				return view("affichage_mariage", $donnees);
			} else {
				return redirect()->to(site_url('/'));
			}
		} else
			return redirect()->to(site_url('/'));
	}

	public function enregistrer_commentaire()
	{

		$session = session();
		$commentaires = [
			'nom' => $this->request->getVar('nom'),
			'telephone' => $this->request->getVar('telephone'),
			'message' => $this->request->getVar('message'),
			'id_mariage' => $this->request->getVar('id_mariage'),
		];


		$this->commentaires = new CommentaireModel();

		$id_commentaire = $this->commentaires->insert($commentaires);

		if ($id_commentaire) {
			$session->setFlashdata('commentaire_success', '<i class="fa fa-check"></i> Merci pour votre commentaire .');
			return redirect()->to(site_url("Home/afficher/" . $commentaires['id_mariage'] . '#main'));
		}
	}
}
