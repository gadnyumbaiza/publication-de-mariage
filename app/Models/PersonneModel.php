<?php

namespace App\Models;

use CodeIgniter\Model;

class PersonneModel extends Model
{
    protected $table      = 'tpersonne'; //nom de la table
    protected $primaryKey = 'id_personne'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['nom', 'postnom', 'prenom', 'datenaiss', 'nationalite', 'nompere', 'nommere', 'chefferie', 'province', 'ville', 'numero', 'adresse', 'commune', 'territoire', 'profession', 'avenue','photo'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
