<?php

namespace App\Models;

use CodeIgniter\Model;

class PayementModel extends Model
{
    protected $table      = 'tpayement'; //nom de la table
    protected $primaryKey = 'id_payement'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['date_payement', 'montant', 'id_mariage'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
