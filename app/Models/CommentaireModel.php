<?php

namespace App\Models;

use CodeIgniter\Model;

class CommentaireModel extends Model
{
    protected $table     = 'tcommentaire'; //nom de la table
    protected $primaryKey = 'id_commentaire'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['message', 'telephone', 'nom', 'id_mariage'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppression';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
