<?php

namespace App\Models;
use CodeIgniter\Model;

class MariageModel extends Model
{
    protected $table     = 'tmariage'; //nom de la table
    protected $primaryKey = 'id_mariage'; // cle primaire
    protected $useSoftDeletes = true;

    protected $allowedFields = ['epoux', 'epouse', 'etat_mariage', 'date_celebration', 'date_publication', 'id_utilisateur','date_rupture'];

    protected $useTimestamps = true;
    protected $createdField  = 'date_creation';
    protected $updatedField  = 'date_modification';
    protected $deletedField  = 'date_suppresion';

    protected $validationRules    = [];
    protected $validationMessages = [];
    protected $skipValidation     = false;
}
