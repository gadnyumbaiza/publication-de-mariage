# Application de publication des mariages

L'application est réalisée à l'aide de PHP et du Framework [CodeIgniter4](https://github.com/codeigniter4/CodeIgniter4)

[[_TOC_]]

## Authentification

![alt text](public/uploads/interface_connexion.png "Interface de connexion")

# Espace Administrateur
## Accueil

![alt text](public/uploads/accueil.png "Interface d'accueil")

## Enregistrement des mariages

![alt text](public/uploads/ajout_mariage.png "Interface d'enregistrement des mariages")

## Liste_mariage des mariages

![alt text](public/uploads/liste_mariage.png "Interface d'affichage des mariages")

## Details  des mariages

![alt text](public/uploads/details_mariage.png "Interface des details")

## Payement 

![alt text](public/uploads/payement.png "Interface de payement")

## Profile 

![alt text](public/uploads/profile.png "Interface de profile")




# Espace Internaute

## Accueil du site

![alt text](public/uploads/accueil_site.png "Interface d'accueil du site")

## Liste de publication sur le site

![alt text](public/uploads/lsite_publication_site.png "Liste de publication sur le site")

## Details sur le site

![alt text](public/uploads/detail_publication_site.png "Interface des details sur le site")
