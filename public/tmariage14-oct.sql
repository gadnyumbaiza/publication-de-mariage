-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : jeu. 14 oct. 2021 à 17:34
-- Version du serveur : 10.4.20-MariaDB
-- Version de PHP : 8.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `mariage`
--

-- --------------------------------------------------------

--
-- Structure de la table `tmariage`
--

CREATE TABLE `tmariage` (
  `epoux` int(11) DEFAULT NULL,
  `epouse` int(11) DEFAULT NULL,
  `date_publication` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `date_celebration` date NOT NULL,
  `date_rupture` date DEFAULT NULL,
  `etat_mariage` varchar(20) DEFAULT NULL,
  `id_mariage` int(11) NOT NULL,
  `id_utilisateur` int(11) NOT NULL,
  `date_creation` date NOT NULL DEFAULT current_timestamp(),
  `date_modification` date DEFAULT NULL,
  `date_suppresion` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `tmariage`
--

INSERT INTO `tmariage` (`epoux`, `epouse`, `date_publication`, `date_celebration`, `date_rupture`, `etat_mariage`, `id_mariage`, `id_utilisateur`, `date_creation`, `date_modification`, `date_suppresion`) VALUES
(1, 2, '2021-10-14 13:19:38', '2021-10-27', NULL, 'publié', 1, 0, '2021-10-14', '2021-10-14', '2021-10-14'),
(4, 3, '2021-10-14 13:19:35', '2021-10-31', NULL, 'publié', 2, 0, '2021-10-14', '2021-10-14', '2021-10-14'),
(6, 5, '2021-10-14 13:33:32', '2021-10-23', NULL, 'en attente', 3, 0, '2021-10-14', '2021-10-14', NULL);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `tmariage`
--
ALTER TABLE `tmariage`
  ADD PRIMARY KEY (`id_mariage`),
  ADD KEY `fk_utilisateur2` (`id_utilisateur`),
  ADD KEY `fk_personne1` (`epoux`),
  ADD KEY `fk_personne2` (`epouse`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `tmariage`
--
ALTER TABLE `tmariage`
  MODIFY `id_mariage` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
